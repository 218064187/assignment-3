### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 02f168b8-12f6-48f2-ac26-424ab0ac0fed
function get_action(actions)
    weights = [a[2] for a in actions]
    actions = [a[1] for a in actions]
    choice = rand(1:length(action))
    return actions[choice]
end;

# ╔═╡ a40a661e-2e84-466f-a7ae-5a750528bb63
function utility(s, p)
# 	
    # which is defined only on terminal states, to give the payoff for each player 
end;

# ╔═╡ 529f2c90-dca5-11eb-2c55-9debe83d3c17
begin	
const players = (player1="congress", player2="press") # a finite set of players
const terminals = ["sign", "veto"]	
const Z = Dict( # terminal nodes node => payoffs
    ("A", "sign") =>  (4, 1),
    ("A", "veto") => (2, 2),

    ("B", "sign") => (1, 4),
    ("B", "veto") => (2, 2),

    ("A+B", "sign") => (3, 3),
    ("A+B", "veto") => (2, 2),
    (players.player1, "nothing") => (2, 2))

game_tree = Dict(
    1 => Dict(players.player1 => [("A", .4), ("B", .1), ("A+B", .3), ("nothing", .2)]),
    2 => Dict(players.player2 =>Dict(
                        # "nothing" => [(2, 2)],
                         "A" => [("sign", .3), ("veto", .7)],
                         "B" => [("sign", .7, (1, 4)), ("veto", .3, (2, 2))],
                         "A+B" => [("sign", .7, (3, 3)), ("veto", .3, (2, 2))]))
)
	
end;

# ╔═╡ 17ef76a2-6496-45e7-ba3d-dd64c20c0572
function actions(s)
    # enumerating the possible actions
    choice = s.choice
    if choice == 1
        return game_tree[choice][s.player]
    end
    return game_tree[choice][s.player][s.action]
end;

# ╔═╡ 8f6ed609-a12b-472b-8c9d-1a8d6e3d4543
function  player(s)
    # returns which player has the move
    return findfirst(p -> p != s.player, players)
end;

# ╔═╡ fe2748a2-3a0f-4ac5-b13f-5da15eb860cb
function successor(s)
    action = get_action(actions(s))
    p = player(s.player)
    choice = s.choice + 1
    return (player=p, choice=choice, action=action)
end;

# ╔═╡ 100b8a30-6a88-4943-a3f5-c6c717eab7e1
function get_players_best_action(action, choice, player)
    best_action = nothing
    if player == players.player2
        actions = game_tree[choice][player][action]
        best_action = actions[1]
        for action in actions
            if action[2] > best_action[2]
                best_action = action
            end
        end
    end
    return best_action
end

# ╔═╡ 5092cd7b-8c2e-4f26-ade1-c60d899062f2
function compute_strategy(player)

    if player == players.player1
        vals = Dict()
        payoffs = Dict()
        for action in game_tree[1][player]
            if haskey(game_tree[2][players.player2], action[1])
                p2_best_action = get_players_best_action(action[1], 2, players.player2)
                payoffs[(action[1], p2_best_action[1])] = Z[(action[1], p2_best_action[1])]
            else
                payoffs[(player, action[1])] = Z[(player, action[1])]
            end

        end

        best_payoff = collect(payoffs)[1]
        for payoff in collect(payoffs)
            if payoff[2][1] >  best_payoff[2][1]
                best_payoff = payoff
            end
        end

        return best_payoff
    end


    if player == players.player2

        payoffs = Dict()
        for action in game_tree[1][players.player1]

            if haskey(game_tree[2][players.player2], action[1])
                payoffs[action[1]] = get_players_best_action(action[1], 2, players.player2)[1]

            end

        end
        return payoffs


    end

end

# ╔═╡ 26759c62-e9d7-4b88-b917-d11e03165fb0
compute_strategy("congress")

# ╔═╡ 499f3561-f502-4966-bfbd-c9ed1640b252
compute_strategy("press")

# ╔═╡ Cell order:
# ╠═17ef76a2-6496-45e7-ba3d-dd64c20c0572
# ╠═02f168b8-12f6-48f2-ac26-424ab0ac0fed
# ╠═8f6ed609-a12b-472b-8c9d-1a8d6e3d4543
# ╠═fe2748a2-3a0f-4ac5-b13f-5da15eb860cb
# ╠═a40a661e-2e84-466f-a7ae-5a750528bb63
# ╠═529f2c90-dca5-11eb-2c55-9debe83d3c17
# ╠═100b8a30-6a88-4943-a3f5-c6c717eab7e1
# ╠═5092cd7b-8c2e-4f26-ade1-c60d899062f2
# ╠═26759c62-e9d7-4b88-b917-d11e03165fb0
# ╠═499f3561-f502-4966-bfbd-c9ed1640b252
