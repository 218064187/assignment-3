### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 2a4624e8-219f-416d-9ef9-68aadb065f7d
begin
# states = [] # a set of state
terminals = [] # a set of terminal nodes
# actions = Dict() # state => [actions]
# transitions = Dict(Dict()) # [state][action] => (prop, next_state, reward)
discount_factor = .9
end_state = nothing # if there is any
threshold = 0.001
end;

# ╔═╡ a2782414-181b-4053-ae42-4bace9ba2eb3
function  isEnd(s)
    return s == end_state
end;

# ╔═╡ fb3fc083-6445-4db0-be15-c9943da68d15
# Example - Solving an MDP
begin
states = ["high", "low"]
	
actions = Dict(
    "high" => ["search", "wait"],
    "low" => ["recharge", "search", "wait"])

transitions = Dict(
    "high" => Dict("search" => [(.7, "high", 4), (.3, "low", 4)], 
                   "wait" => [(1., "high", 1)]),

    "low" => Dict("search" => [(.8, "high", -3), (.2, "low", 4)], 
                   "wait" => [(1., "low", 1)],
                   "recharge" => [(1., "high", 0)]))
end;

# ╔═╡ 04330e52-6982-4463-a47f-6266d822184c
function get_actions(s)
    return s in terminals ? [nothing] : actions[s]
end;

# ╔═╡ c6441364-b89f-4e8f-b9fd-1243175ac35a
const mdp = (
    states=states,
    actions=get_actions,
    transitions=transitions,
    discount_factor=discount_factor);

# ╔═╡ fa22694a-5ed7-4b30-817b-bafbcba2b632
function calc_max(s, V)
    # calculate the value for all action in a given state 
    # & return max(values) and an action of that v
    vals = Dict()
    for a in mdp.actions(s)
        value = sum(p * (r + discount_factor * V[ns])
                    for (p, ns, r) in  mdp.transitions[s][a])
        vals[value] = a
    end
    max_val = max(keys(vals)...)
    return max_val, vals[max_val]
end;

# ╔═╡ 3223706e-dca5-11eb-147b-f727e75a04fb
function Value_Iteration(mdp, theta=threshold)
    V = Dict(s => 0. for s in mdp.states)
    while true
        V1 = Dict()
        for s in mdp.states
            # update values if you're in the end state the value = 0
            V1[s] = isEnd(s) ? 0 : calc_max(s, V)[1]
        end

         # check for convergency | check how much the values changed
        if max([abs(V[s] - V1[s]) for s in mdp.states]...) <= theta
            break
        end
        V = V1
    end

    # obtain the policy corresponding to the final value function
    PI = Dict()
    for s in mdp.states
        PI[s] = isEnd(s) ? nothing : calc_max(s, V)[2]
    end
    return PI, V
end;

# ╔═╡ dc6bef33-f34d-4623-b6ad-fa84e67c5ba0
policy, value_function = Value_Iteration(mdp);

# ╔═╡ 8107d9b8-22b3-40ff-b1b9-0b02c9305c83
policy

# ╔═╡ 379e47ef-ff0c-47fc-ade2-3e969ca20c22
value_function

# ╔═╡ Cell order:
# ╠═3223706e-dca5-11eb-147b-f727e75a04fb
# ╠═fa22694a-5ed7-4b30-817b-bafbcba2b632
# ╠═04330e52-6982-4463-a47f-6266d822184c
# ╠═a2782414-181b-4053-ae42-4bace9ba2eb3
# ╠═2a4624e8-219f-416d-9ef9-68aadb065f7d
# ╠═fb3fc083-6445-4db0-be15-c9943da68d15
# ╠═c6441364-b89f-4e8f-b9fd-1243175ac35a
# ╠═dc6bef33-f34d-4623-b6ad-fa84e67c5ba0
# ╠═8107d9b8-22b3-40ff-b1b9-0b02c9305c83
# ╠═379e47ef-ff0c-47fc-ade2-3e969ca20c22
